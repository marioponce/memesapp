package com.example.marioponce.memesapp.features.login.presenter

import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.firebase.auth.FirebaseUser

interface LoginPresenter {

    fun setView(view: View)
    fun registerGoogleAccountInFirebase (account: GoogleSignInAccount)

    interface View {
        fun onSuccess()
        fun onError()
    }
}