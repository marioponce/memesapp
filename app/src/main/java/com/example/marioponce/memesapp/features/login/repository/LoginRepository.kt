package com.example.marioponce.memesapp.features.login.repository

import com.google.android.gms.auth.api.signin.GoogleSignInAccount

interface LoginRepository {
    fun signInWithGoogleCredentials (acct : GoogleSignInAccount)
}