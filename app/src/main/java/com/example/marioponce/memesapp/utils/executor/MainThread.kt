package com.example.marioponce.memesapp.utils.executor

interface MainThread {
    fun post(runnable: Runnable)
}