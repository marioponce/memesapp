package com.example.marioponce.memesapp.features.login.interactor

import com.example.marioponce.memesapp.features.login.repository.impl.LoginRepositoryImpl
import com.example.marioponce.memesapp.utils.MemesBaseInteractor
import com.google.android.gms.auth.api.signin.GoogleSignInAccount


class LoginInteractor(private val googleSignInAccount: GoogleSignInAccount) : MemesBaseInteractor(), Runnable, LoginRepositoryImpl.SignInWithGoogleCredentialsCallback {

    private lateinit var callback: LoginInteractorCallback
    private val loginRepositoryImpl : LoginRepositoryImpl = LoginRepositoryImpl(this)

    fun execute(callback: LoginInteractorCallback) {
        this.callback = callback
        executor.execute(this)
    }

    override fun run() {
        firebaseAuthWithGoogle(googleSignInAccount)
    }

    private fun firebaseAuthWithGoogle(acct: GoogleSignInAccount) {
        loginRepositoryImpl.signInWithGoogleCredentials(acct)
    }

    override fun onError() {
        callback.onError()
    }

    override fun onSuccess() {
        callback.onSuccess()
    }

    interface LoginInteractorCallback {
        fun onSuccess()
        fun onError()
    }
}