package com.example.marioponce.memesapp.features.register

import android.os.Bundle
import com.example.marioponce.memesapp.R
import com.example.marioponce.memesapp.utils.MemesBaseActivity

class RegisterActivity : MemesBaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
    }
}
