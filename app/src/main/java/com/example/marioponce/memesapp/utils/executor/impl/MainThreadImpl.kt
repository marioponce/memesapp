package com.example.marioponce.memesapp.utils.executor.impl

import android.os.Handler
import com.example.marioponce.memesapp.utils.executor.MainThread
import android.os.Looper



class MainThreadImpl : MainThread {

    private val handler: Handler = Handler(Looper.getMainLooper())

    override fun post(runnable: Runnable) {
        handler.post(runnable)
    }
}