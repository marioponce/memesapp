package com.example.marioponce.memesapp.utils

import com.example.marioponce.memesapp.utils.executor.impl.MainThreadImpl
import java.util.concurrent.Executor
import java.util.concurrent.Executors

abstract class MemesBaseInteractor {
    val mainThreadImpl : MainThreadImpl = MainThreadImpl()
    val executor : Executor = Executors.newSingleThreadExecutor()
}