package com.example.marioponce.memesapp.features.login.repository.impl

import android.util.Log
import com.example.marioponce.memesapp.features.login.repository.LoginRepository
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider

class LoginRepositoryImpl(private val callback: SignInWithGoogleCredentialsCallback) : LoginRepository {

    private val TAG = "LoginRepositoryImpl"
    private val fireBaseAuth : FirebaseAuth = FirebaseAuth.getInstance()

    override fun signInWithGoogleCredentials(acct: GoogleSignInAccount) {
        Log.d(TAG, "firebaseAuthWithGoogle:" + acct.id)

        val credential = GoogleAuthProvider.getCredential(acct.idToken, null)
        fireBaseAuth.signInWithCredential(credential).addOnCompleteListener { task ->
            if (task.isSuccessful) { // Sign in success
                val user = fireBaseAuth.currentUser
                user?.let {
                    Log.d(TAG, "signInWithCredential:success")
                    callback.onSuccess()
                }
            } else { // Sign in error
                Log.w(TAG, "signInWithCredential:failure", task.exception)
                callback.onError()
            }
        }
    }

    interface SignInWithGoogleCredentialsCallback {
        fun onSuccess()
        fun onError()
    }
}