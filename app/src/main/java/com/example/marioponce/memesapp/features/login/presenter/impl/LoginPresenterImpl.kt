package com.example.marioponce.memesapp.features.login.presenter.impl

import com.example.marioponce.memesapp.features.login.interactor.LoginInteractor
import com.example.marioponce.memesapp.features.login.presenter.LoginPresenter
import com.google.android.gms.auth.api.signin.GoogleSignInAccount

class LoginPresenterImpl : LoginPresenter, LoginInteractor.LoginInteractorCallback {

    private lateinit var view : LoginPresenter.View
    override fun setView(view: LoginPresenter.View) {
        this.view = view
    }

    override fun registerGoogleAccountInFirebase(account: GoogleSignInAccount) {
        val interactor = LoginInteractor(account)
        interactor.execute(this)
    }

    override fun onError() {
        view.onError()
    }

    override fun onSuccess() {
        view.onSuccess()
    }
}