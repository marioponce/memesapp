package com.example.marioponce.memesapp.utils

object MemesConstants {

    val WEB_ID = "141903136656-rul6sv9gcpdirktknkn5sqsd2ro2ctk9.apps.googleusercontent.com"
    val RC_SIGN_IN = 1001
    val RC_REGISTER = 2001
    val RC_MAIN_MEMES = 3001

}